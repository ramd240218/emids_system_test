package test.model;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private String name;
	private int age;
	private List<Habit> habits;
	private Gender gender;
	private List<Condition> conditions;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public List<Habit> getHabits() {
		if(habits != null && !habits.isEmpty()) {
			return habits;
		} else {
			return new ArrayList<Habit>();
		}
	}
	public void setHabits(List<Habit> habits) {
		this.habits = habits;
	}
	
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public List<Condition> getConditions() {
		return conditions;
	}
	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}
}
