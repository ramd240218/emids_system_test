package test.model;

public enum Habit {
	ALCOHOL ("alcohol"),
	SMOKE ("smoke"),
	DRUGs ("drugs"),
	DAILY_EXERCISES("daily exercises");
	
	private String habit;
	private Habit(String habit) {
		this.habit = habit;
	}
	
	public String getHabit() {
		return habit;
	}
	public void setHabit(String habit) {
		this.habit = habit;
	}

	public static boolean isBadHabit(Habit habit) {
		if(habit.getHabit().matches("alcohol|smoke|drugs")) {
			return true;
		}
		return false;
	}
	
	public static Habit getValue(String value) {
		for(Habit habit : Habit.values()) {
			if(habit.getHabit().equalsIgnoreCase(value)) {
				return habit;
			}
		}
		return null;
	}
}
