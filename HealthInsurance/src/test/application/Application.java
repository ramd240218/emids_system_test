package test.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import test.model.Condition;
import test.model.Gender;
import test.model.Habit;
import test.model.Person;
import test.result.Premium;
import test.rule.RuleExecutor;

public class Application {
	public static void main (String args[]) {
		Scanner scanner = new Scanner(System.in);
		Person person = new Person();
		
		System.out.println("Please enter name:");
		person.setName(scanner.nextLine());
		scanner.reset();

		System.out.println("Please enter gender:");
		Gender gender = Gender.valueOf(scanner.nextLine().toUpperCase());
		person.setGender(gender);
		scanner.reset();
		
		System.out.println("Please enter age:");
		person.setAge(scanner.nextInt());
		scanner.reset();

		String habitList = "alcohol,daily exercises";
		List<Habit> habits = new ArrayList<Habit>();
		for(String habitString : habitList.split(",")) {
			habits.add(Habit.getValue(habitString));
		}
		person.setHabits(habits);
		scanner.reset();
		
		String[] conditionArray = "Overweight".split(",");
		List<Condition> conditions = new ArrayList<Condition>();
		for(String conditionString : conditionArray) {
			conditions.add(new Condition(conditionString));
		}
		person.setConditions(conditions);
		
		Premium premium = new RuleExecutor(person).getPremium();
		System.out.println("Health Insurance Premium for " + person.getName() + ": Rs." + premium.getAmount());
		scanner.close();
	}
}
