package test.result;

public class Premium {
	private double amount;
	private static int slab;
	private static Premium instance;
	
	private Premium (int age) {
		if(age < 18) {
			slab = 0;
		} else if(18 < age && age < 25) {
			slab = 1;
		} else if(age > 25) {
			int slabBase = age - 25;
			int div = slabBase / 5;
			int mod = age % 5;
			if (mod != 0) {
				slab = div + 2;
			} else {
				slab = div;
			}
		}
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public static int getSlab() {
		return slab;
	}
	
	public static Premium getInstance(int age) {
		if(instance != null) {
			return instance;
		} else {
			instance = new Premium(age);
		}
		return instance;
	}
}
