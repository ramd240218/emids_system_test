package test.rule;

import test.model.Person;
import test.result.Premium;

public class RuleExecutor {
	private Premium premium;
	
	public RuleExecutor(Person person) {
		execute(person);
	}
	
	public void execute(Person person) {
		Premium premium = Premium.getInstance(person.getAge());
		premium.setAmount(Rules.apply(person));
		this.setPremium(premium);
	}

	public Premium getPremium() {
		return premium;
	}
	public void setPremium(Premium premium) {
		this.premium = premium;
	}
}
