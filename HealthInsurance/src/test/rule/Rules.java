package test.rule;

import test.model.Condition;
import test.model.Gender;
import test.model.Habit;
import test.model.Person;
import test.result.Premium;
import test.util.Calculate;
import test.util.PremiumCalculator;

public class Rules {
	private static final double BASE_PREMIUM = 5000;
	private static final String PERSON_CONDTIONS = "Hypertension|Blook pressure|Blood sugar|Overweight";
	public static double apply(Person person) {
		int slab = Premium.getSlab();
		double premiumAmount = BASE_PREMIUM;
		
		for (int i = 1; i <= slab; i++) {
			if (person.getAge() < 40) {
				premiumAmount = PremiumCalculator.calculate(premiumAmount, 10, Calculate.INCREASE);
			} else {
				premiumAmount =  PremiumCalculator.calculate(premiumAmount, 20, Calculate.INCREASE);
			}
			if (person.getGender() == Gender.MALE) {
				premiumAmount = PremiumCalculator.calculate(premiumAmount, 2, Calculate.INCREASE);
			}
			for(Habit habit : person.getHabits()) {
				if(Habit.isBadHabit(habit)) {
					premiumAmount = PremiumCalculator.calculate(premiumAmount, 3, Calculate.INCREASE);
				} else {
					premiumAmount = PremiumCalculator.calculate(premiumAmount, 3, Calculate.REDUCE);
				}
			}
			
			for(Condition condition : person.getConditions()) {
				if(condition.getName().matches(PERSON_CONDTIONS)) {
					premiumAmount = PremiumCalculator.calculate(premiumAmount, 3, Calculate.INCREASE);
				}
			}
		}
		return premiumAmount;
	}
}
