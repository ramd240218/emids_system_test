package test.util;

public class PremiumCalculator {
	
	public static double calculate(double amount, int percentage, Calculate type) {
		if(type == Calculate.INCREASE) {
			amount = amount + ((amount/100) * percentage);
		} else {
			amount = amount - ((amount/100) * percentage);
		}
		return amount;
	}
}
